package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	// Get all users
	@GetMapping("/users")
	public String getAllUsers(){
		return "All users retrieved";
	}

	// Create User
	@PostMapping("/users")
	public String createUser(){
		return "New user created" ;
	}

	//Retrieving a specific user
	@GetMapping("/users/{userId}")
	public String getUserByUserId(@PathVariable Long userId){
		return "The user with the ID " +userId+ " is being retrieved.";
	}

	// Delete a user
	@DeleteMapping("/users/{userId}")
	public String deleteUser(@PathVariable Long userId, @RequestHeader(value = "Authorization", required = false) String authorizationHeader){
		if (authorizationHeader == null || authorizationHeader.isEmpty()) {
			return "Unauthorized access";
		}
		return "The user " + userId + " has been deleted";
	}

	// Update a user
	@PutMapping("/users/{userId}")
	@ResponseBody
	public User updateUser(@PathVariable Long userId, @RequestBody User user){
		return user;
	}
}

